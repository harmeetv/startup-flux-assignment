// 1 a
function reverseString(str) {
	return str.split('').reverse().join('');
}

// 1 b
function reverseWords(sentence){
  return sentence.split(" ").map(word => reverseString(word)).join(" ");
}

// 1 c
function reverseOnSpecialChars(sentence) {
  let newStr = "";
  let strToRev = "";
  sentence.split("").forEach(c => {
    if (["#","$","%","@","*","!"].includes(c)) {
      newStr += reverseString(strToRev)+c;
      strToRev="";
    }
    else {
      strToRev += c;
    }
  });
  newStr += reverseString(strToRev);
  return newStr
}

// 2
function countOfOddNumber(arr) {
	return arr.reduce((count, num) => (count + Math.abs(num)%2), 0);
}

// 3
function moveSpecialCharsToEnd(arr) {
	let intArr = [];
  let spCharArr = [];
  arr.forEach(item => {
  	if(isNaN(item)) {
    	spCharArr.push(item);
    }
    else {
    	intArr.push(item);
    }
  });
  return [...intArr, ...spCharArr];
}


// Event Listeners
document.getElementById("reverseStringBtn").addEventListener("click", function(){
  document.getElementById("reverseStringAns").innerHTML = reverseString(document.getElementById("reverseString").value);
});
document.getElementById("reverseWordsBtn").addEventListener("click", function(){
  document.getElementById("reverseWordsAns").innerHTML = reverseWords(document.getElementById("reverseWords").value);
});
document.getElementById("reverseOnSpecialCharsBtn").addEventListener("click", function(){
  document.getElementById("reverseOnSpecialCharsAns").innerHTML = reverseOnSpecialChars(document.getElementById("reverseOnSpecialChars").value);
});
document.getElementById("countOfOddNumberBtn").addEventListener("click", function(){
  document.getElementById("countOfOddNumberAns").innerHTML = countOfOddNumber(document.getElementById("countOfOddNumber").value.split(","));
});
document.getElementById("moveSpecialCharsToEndBtn").addEventListener("click", function(){
  document.getElementById("moveSpecialCharsToEndAns").innerHTML = moveSpecialCharsToEnd(document.getElementById("moveSpecialCharsToEnd").value.split(","));
});

